package com.sauloaraujo.timesheet.test;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import com.sauloaraujo.timesheet.domain.entry.Entry;
import com.sauloaraujo.timesheet.domain.entry.EntryRepository;
import com.sauloaraujo.timesheet.domain.project.Project;
import com.sauloaraujo.timesheet.domain.project.ProjectRepository;
import com.sauloaraujo.timesheet.domain.project.Task;
import com.sauloaraujo.timesheet.domain.project.TaskRepository;
import com.sauloaraujo.timesheet.domain.user.User;
import com.sauloaraujo.timesheet.domain.user.UserRepository;

@Named
public class Populator {
	private @Inject EntryRepository entryRepository;
	private @Inject ProjectRepository projectRepository;
	private @Inject TaskRepository taskRepository;
	private @Inject UserRepository userRepository;

	@PostConstruct
	@Transactional
	public void populate() {
		Date now = new Date();
		
		User user = new User();
		user.setName("User 1");
		userRepository.save(user);

		int taskId = 1;
		for (int i = 1; i <= 10; ++i) {
			Project project = new Project();
			project.setName("Project " + i);
			projectRepository.save(project);
			
			for (int j = 1; j <= 10; ++j) {
				Task task = new Task();
				task.setName("Task " + taskId);
				task.setProject(project);
				taskRepository.save(task);
				
				Entry entry = new Entry();
				entry.setUser(user);
				entry.setDate(now);
				entry.setTask(task);
				entry.setTime(1);
				entryRepository.save(entry);
				
				++taskId;
			}
		}
	}
}
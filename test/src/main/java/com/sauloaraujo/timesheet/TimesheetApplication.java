package com.sauloaraujo.timesheet;

import java.util.TimeZone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

import com.sauloaraujo.timesheet.ws.WebSocket;

@SpringBootApplication
public class TimesheetApplication {
	@Bean
	public WebSocket webSocket() {
		return new WebSocket();
	}
	
	@Bean
	public ServerEndpointExporter endpointExporter() {
	    return new ServerEndpointExporter();
	}
    
	public static void main(String[] args) {
		TimeZone utc = TimeZone.getTimeZone("UTC");
		TimeZone.setDefault(utc);

		SpringApplication.run(TimesheetApplication.class, args);
	}
}
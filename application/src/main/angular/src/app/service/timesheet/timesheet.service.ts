import { Injectable } from '@angular/core'
import { Http } from '@angular/http'

import { filter, last, range } from 'lodash'

import * as moment from 'moment'

@Injectable()
export class TimesheetService {
  constructor(private http: Http) { }

  getTimesheet(userId: number, search: any): Promise<ITimesheet> {
    const startParameter = search["start"]
    const start = startParameter ? parseInt(startParameter) : moment().utc().hour(0).minute(0).second(0).millisecond(0).valueOf()

    const daysParameter = search["days"]
    const days = daysParameter ? parseInt(daysParameter) : 7

    const header = range(days).map(days => moment(start).utc().add(days, "day").valueOf())

    const end = last(header)

    const projectProjectionsPromise = this.http.get('rest/spring/projects', {
      params: {
        projection: "projectProjection"
      }
    }).toPromise().then(response => response.json() as IProjectProjections)

    const entryProjectionsPromise = this.http.get('/rest/spring/entries/search/findByUserIdAndDateBetween', {
      params: {
        userId: userId,
        start: moment(start).utc().toISOString(),
        end: moment(end).utc().toISOString(),
        projection: "entryProjection"
      }
    }).toPromise().then(response => response.json() as IEntryProjections)

    return Promise.all([projectProjectionsPromise, entryProjectionsPromise]).then(responses => {
      const projectProjections = responses[0]
      const entryProjections = responses[1]

      const projectRows = projectProjections._embedded.projects.map(projectProjection => {
        return {
          project: {
            name: projectProjection.name
          },
          taskRows: projectProjection.tasks.map(taskProjection => {
            return {
              task: {
                id: taskProjection.id,
                name: taskProjection.name
              },
              entryCells: header.map(date => {
                const entryProjection = entryProjections._embedded.entries.find(entryProjection => {
                  return entryProjection.taskId === taskProjection.id && entryProjection.date === date
                })
                const time = entryProjection ? entryProjection.time : null
                return {
                  entry: {
                    time: time
                  }
                }
              })
            }
          })
        }
      })

      return {
        header: header,
        projectRows: projectRows
      }
    })
  }

  saveEntry(userId: number, taskId: number, date: number, time: number) {
    const url = '/rest/spring/entries/search/findByUserIdAndTaskIdAndDate'
    const options = {
      params: {
        userId: userId,
        taskId: taskId,
        date: moment(date).utc().toISOString(),
        projection: "entryProjection"
      }
    }
    this.http.get(url, options).toPromise()
      .then(response => {
        const entryProjection = response.json() as IEntryProjection
        const url = `/rest/spring/entries/${entryProjection.id}`
        const entry = {
          time: time
        }
        this.http.patch(url, entry).toPromise()
      })
      .catch(() => {
        const url = `/rest/spring/entries`
        const entry = {
          user: '/rest/spring/users/' + userId,
          task: '/rest/spring/tasks/' + taskId,
          date: date,
          time: time
        }
        this.http.post(url, entry).toPromise()
      })
  }

  getTimesheet2(userId: number, search: any): Promise<ITimesheet> {
    const url = `rest/jersey/user/${userId}/timesheet/edit`
    const options = {
      search: search
    }
    return this.http.get(url, options).toPromise().then(response => response.json())
  }

  saveTimesheet(userId: number, timesheet: ITimesheet): Promise<any> {
    const url = `rest/jersey/user/${userId}/timesheet/save`
    return this.http.post(url, timesheet).toPromise().then(response => response.json())
  }
}
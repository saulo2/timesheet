import { BrowserModule } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { FormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'
import { NgModule } from '@angular/core'
import { NgxChartsModule } from '@swimlane/ngx-charts'
import { RouterModule, Routes } from '@angular/router'

import { AppComponent } from './component/app/app.component'
import { EditTimesheetComponent, EditTimesheetResolve, editTimesheetResolve } from './component/edit-timesheet/edit-timesheet.component';
import { FocusDirective } from './directive/focus/focus.directive'
import { TimesheetChartComponent } from './component/timesheet-chart/timesheet-chart.component'
import { TimesheetService } from './service/timesheet/timesheet.service'
import { SearchHeaderComponent } from './component/search-header/search-header.component'
import { WindowComponent } from './component/window/window.component'

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'user/1/timesheet/edit'
  },
  {
    path: 'user/:userId/timesheet/edit',
    component: EditTimesheetComponent,
    resolve: editTimesheetResolve
  }
]

@NgModule({
  declarations: [
    AppComponent,
    EditTimesheetComponent,
    WindowComponent,
    TimesheetChartComponent,
    SearchHeaderComponent,
    FocusDirective
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routes),
    NgxChartsModule
  ],
  providers: [
    EditTimesheetResolve,
    TimesheetService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'ts-timesheet-chart',
  templateUrl: './timesheet-chart.component.html',
  styleUrls: ['./timesheet-chart.component.css']
})
export class TimesheetChartComponent implements OnChanges {
  @Input() timesheet: ITimesheet

  chart: {
    name: string,
    value: number
  }[]

  colors = {
    domain: ["#9e0142", "#d53e4f", "#f46d43", "#fdae61", "#fee08b", "#ffffbf", "#e6f598", "#abdda4", "#66c2a5", "#3288bd", "#5e4fa2", []]
  }

  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.timesheet !== null) {
      this.chart = this.timesheet.projectRows.map(projectRow => {
        return {
          name: projectRow.project.name,
          value: projectRow.taskRows.map(taskRow => {
            return taskRow.entryCells.reduce((taskRowTotal, entryCell) => {
              const time = entryCell.entry.time
              return taskRowTotal + (time !== null ? time : 0)
            }, 0)
          }).reduce((subTotal, projectRowTotal) => {
            return subTotal + projectRowTotal
          }, 0)
        }
      })
    } else {
      this.chart = null
    }
  }
}
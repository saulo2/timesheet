import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimesheetChartComponent } from './timesheet-chart.component';

describe('TimesheetChartComponent', () => {
  let component: TimesheetChartComponent;
  let fixture: ComponentFixture<TimesheetChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimesheetChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimesheetChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

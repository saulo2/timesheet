import { Component, Injectable, OnDestroy, OnInit } from '@angular/core'
import { Http } from '@angular/http'
import { ActivatedRoute, ActivatedRouteSnapshot, Resolve, Router } from '@angular/router'

import 'rxjs/add/operator/skip'

import * as moment from 'moment'

import { SearchHeaderComponent } from '../search-header/search-header.component'
import { TimesheetService } from '../../service/timesheet/timesheet.service'

@Injectable()
export class EditTimesheetResolve implements Resolve<ITimesheet> {
  constructor(private service: TimesheetService) { }

  resolve(route: ActivatedRouteSnapshot): Promise<ITimesheet> {
    return this.service.getTimesheet(route.params.userId, route.queryParams)
  }
}

@Component({
  selector: 'ts-edit-timesheet',
  templateUrl: './edit-timesheet.component.html',
  styleUrls: ['./edit-timesheet.component.css'],
})
export class EditTimesheetComponent implements OnInit, OnDestroy {
  static readonly USER_ID = "userId"
  static readonly TIMESHEET = "timesheet"

  userId: number
  timesheet: ITimesheet
  webSocket: WebSocket

  constructor(private route: ActivatedRoute, private router: Router, private service: TimesheetService) { }

  ngOnInit(): void {
    this.userId = this.route.snapshot.params[EditTimesheetComponent.USER_ID]
    this.timesheet = this.route.snapshot.data[EditTimesheetComponent.TIMESHEET]

    this.route.queryParams.skip(1).subscribe(queryParams => {
      this.service.getTimesheet(this.userId, queryParams).then(timesheet => this.timesheet = timesheet)
    })

    this.webSocket = new WebSocket("ws://localhost:4200/ws")

    this.webSocket.onopen = (event) => {
      console.log(event)
    }

    this.webSocket.onmessage = (event) => {
      const timesheet  = JSON.parse(event.data) as ITimesheet
      console.log(timesheet)
    }

    this.webSocket.onclose = (event) => {
      console.log(event)
    }

    this.webSocket.onerror = (event) => {
      console.log(event)
    }
  }

  ngOnDestroy(): void {
    this.webSocket.close()
  }

  handleFocus(entryCell: IEntryCell): void {
    entryCell.oldTime = entryCell.entry.time
  }

  handleKeyUp(event: KeyboardEvent): void {
    if (event.keyCode === 13) {
      const target = event.target as HTMLElement
      target.blur()
    }
  }

  handleBlur(task: ITask, entryCell: IEntryCell, entryCellIndex: number): void {
    if (entryCell.entry.time !== entryCell.oldTime) {
      entryCell.state = "saving"

      this.timesheet = {
        header: this.timesheet.header,
        projectRows: this.timesheet.projectRows
      }

      //this.service.saveEntry(this.userId, task.id, this.timesheet.header[entryCellIndex], entryCell.entry.time)

      const entryCells = new Array<IEntryCell>(this.timesheet.header.length)
      entryCells[entryCellIndex] = {
        entry: entryCell.entry
      }

      const timesheet: ITimesheet = {
        header: this.timesheet.header,
        projectRows: [{
          project: {},
          taskRows: [{
            task: {
              id: task.id
            },
            entryCells: entryCells
          }]
        }]
      }

      this.service.saveTimesheet(this.userId, timesheet)
        .then(() => entryCell.state = "initial")
        .catch(() => entryCell.state = "error")
    }
  }

  goToPrevious($event: MouseEvent): void {
    this.goTo($event, false)
  }

  goToNext($event: MouseEvent): void {
    this.goTo($event, true)
  }

  goTo($event: MouseEvent, foward: boolean): void {
    event.preventDefault()

    const startMiliseconds = this.timesheet.header[0]
    const startMoment = moment(startMiliseconds).utc()
    const days = this.timesheet.header.length
    if (foward) {
      startMoment.add(days, "days")
    } else {
      startMoment.subtract(days, "days")
    }
    this.router.navigate([], {
      relativeTo: this.route,
      queryParamsHandling: "merge",
      queryParams: {
        start: startMoment.valueOf()
      }
    })
  }

  hasMoreThanOneDay(): boolean {
    return this.timesheet.header.length > 1
  }

  removeDay($event: MouseEvent): void {
    $event.preventDefault()

    this.router.navigate([], {
      relativeTo: this.route,
      queryParamsHandling: "merge",
      queryParams: {
        days: Math.max(0, this.timesheet.header.length - 1)
      }
    })
  }

  addDay($event: MouseEvent): void {
    $event.preventDefault()

    this.router.navigate([], {
      relativeTo: this.route,
      queryParamsHandling: "merge",
      queryParams: {
        days: this.timesheet.header.length + 1
      }
    })
  }

  projectSearchHeaderMode: SearchHeaderMode = "header"
  projectSearchHeaderValue: string

  isProjectRowVisible = (projectRow: IProjectRow): boolean => {
    if (this.projectSearchHeaderMode === "header") {
      return true
    } else {
      if (this.projectSearchHeaderValue) {
        return projectRow.project.name.toLowerCase().indexOf(this.projectSearchHeaderValue.toLowerCase()) >= 0
      } else {
        return true
      }
    }
  }

  taskSearchHeaderMode: SearchHeaderMode = "header"
  taskSearchHeaderValue: string

  isTaskRowVisible = (taskRow: ITaskRow): boolean => {
    if (this.taskSearchHeaderMode === "header") {
      return true
    } else {
      if (this.taskSearchHeaderValue) {
        return taskRow.task.name.toLowerCase().indexOf(this.taskSearchHeaderValue.toLowerCase()) >= 0
      } else {
        return true
      }
    }
  }

  getProjectRows(): IProjectRow[] {
    return this.timesheet ? this.timesheet.projectRows.filter(this.isProjectRowVisible) : null
  }

  getTaskRows(projectRow: IProjectRow): ITaskRow[] {
    return projectRow.taskRows.filter(this.isTaskRowVisible)
  }
}

export const editTimesheetResolve = {}
editTimesheetResolve[EditTimesheetComponent.TIMESHEET] = EditTimesheetResolve
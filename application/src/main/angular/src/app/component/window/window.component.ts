import { Component, Input } from '@angular/core'

import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations'

@Component({
  selector: 'ts-window',
  templateUrl: './window.component.html',
  styleUrls: ['./window.component.css'],
  animations: [
    trigger('mode', [
      state('collapsed', style({
        'margin-bottom': '-316px'
      })),
      state('expanded', style({
        'margin-bottom': '0px'
      })),
      transition('collapsed <=> expanded', animate('200ms'))
    ])
  ]
})
export class WindowComponent {
  @Input() title: string

  mode: WindowComponentMode = 'collapsed'

  constructor() { }

  collapse($event: MouseEvent): void {
    $event.preventDefault()

    this.mode = 'collapsed'
  }

  isCollapsed(): boolean {
    return this.mode === 'collapsed'
  }

  expand($event: MouseEvent): void {
    $event.preventDefault()

    this.mode = 'expanded'
  }

  isExpanded(): boolean {
    return this.mode === 'expanded'
  }
}

type WindowComponentMode = 'collapsed' | 'expanded'
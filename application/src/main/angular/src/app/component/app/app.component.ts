import { Component } from '@angular/core';

@Component({
  selector: 'ts-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ts';
}

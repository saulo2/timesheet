import { Component, EventEmitter, Input, Output } from '@angular/core'

@Component({
  selector: 'ts-search-header',
  templateUrl: './search-header.component.html',
  styleUrls: ['./search-header.component.css']
})
export class SearchHeaderComponent {
  @Input() mode: SearchHeaderMode = "header"
  @Output() modeChange = new EventEmitter<SearchHeaderMode>()

  @Input() value: string
  @Output() valueChange = new EventEmitter<String>()

  constructor() { }

  isInHeaderMode(): boolean {
    return this.mode === "header"
  }

  isInSearchMode(event: MouseEvent): boolean {
    return this.mode === "search"
  }

  goToHeaderMode(event?: MouseEvent): void {
    if (event) {
      event.preventDefault()
    }

    if (this.mode !== "header") {
      this.mode = "header"

      this.modeChange.emit(this.mode)
    }
  }

  goToSearchMode(event?: MouseEvent): void {
    if (event) {
      event.preventDefault()
    }

    if (this.mode !== "search") {
      this.mode = "search"

      this.modeChange.emit(this.mode)
    }
  }

  handleKeyUp(event: KeyboardEvent): void {
    if (event.keyCode === 27) {
      this.goToHeaderMode()
    } else {
      this.valueChange.emit(this.value)
    }
  }
}
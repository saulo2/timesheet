import { Directive, ElementRef, OnInit } from '@angular/core'

@Directive({
  selector: '[tsFocus]'
})
export class FocusDirective implements OnInit {
  constructor(private reference: ElementRef) { }

  ngOnInit(): void {
    const element = this.reference.nativeElement as HTMLElement
    element.focus()
  }
}
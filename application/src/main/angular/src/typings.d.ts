/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
  id: string;
}

/* ------------------------------------------------------------------------- */
type SearchHeaderMode = "header" | "search" 
/* ------------------------------------------------------------------------- */

/* ------------------------------------------------------------------------- */
interface ITimesheet {
  header: number[]
  projectRows: IProjectRow[]
}

interface IProjectRow {
  project: IProject
  taskRows: ITaskRow[]
}

interface IProject {
  name?: string
}

interface ITaskRow {
  task: ITask
  entryCells: IEntryCell[]
}

interface ITask {
  id: number
  name?: string
}

interface IEntryCell {
  entry: IEntry
  state?: "initial" | "saving" | "error"
  oldTime?: number
}

interface IEntry {
  time: number
}
/* ------------------------------------------------------------------------- */

/* ------------------------------------------------------------------------- */
interface IProjectProjections {
  _embedded: IProjectProjectionsEmbedded
}

interface IProjectProjectionsEmbedded {
  projects: IProjectProjection[]
}

interface IProjectProjection {
  name: string
  tasks: ITaskProjection[]
}

interface ITaskProjection {
  id: number
  name: string
}
/* ------------------------------------------------------------------------- */

/* ------------------------------------------------------------------------- */
interface IEntryProjections {
  _embedded: IEntryProjectionsEmbedded
}

interface IEntryProjectionsEmbedded {
  entries: IEntryProjection[]
}

interface IEntryProjection {
  id: number
  taskId: number
  date: number
  time: number
}
/* ------------------------------------------------------------------------- */
package com.sauloaraujo.timesheet.domain.entry;

import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.format.annotation.DateTimeFormat;

public interface EntryRepository extends JpaRepository<Entry, Long> {
	List<Entry> findByUserIdAndDateBetween(long userId, @DateTimeFormat(iso = DATE) Date start, @DateTimeFormat(iso = DATE) Date end);
	Entry findByUserIdAndTaskIdAndDate(long userId, long taskId, @DateTimeFormat(iso = DATE) Date date);
}
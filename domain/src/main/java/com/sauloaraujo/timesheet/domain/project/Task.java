package com.sauloaraujo.timesheet.domain.project;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
public class Task {
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private Long id;
	
	@NotNull
	@Size(min = 1, max = 50)
	private String name;
	
	@Size(min = 1, max = 200)
	private String description;
	
	@ManyToOne
	@NotNull
	private Project project;
}
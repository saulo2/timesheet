package com.sauloaraujo.timesheet.domain;

public class BusinessException extends RuntimeException {
	private static final long serialVersionUID = 1602848837313064018L;
	
	public BusinessException(String message) {
		super(message); 
	}
}
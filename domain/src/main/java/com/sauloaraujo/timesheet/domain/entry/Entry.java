package com.sauloaraujo.timesheet.domain.entry;

import static javax.persistence.GenerationType.IDENTITY;
import static javax.persistence.TemporalType.DATE;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import com.sauloaraujo.timesheet.domain.project.Task;
import com.sauloaraujo.timesheet.domain.user.User;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"USER_ID", "TASK_ID", "DATE"}))
@Setter
@Getter
public class Entry {
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private Long id;
	
	@ManyToOne
	@NotNull
	private User user;

	@ManyToOne
	@NotNull
	private Task task;
	
	@Temporal(DATE)
	@NotNull
	private Date date;
	
	@NotNull
	private double time;
}
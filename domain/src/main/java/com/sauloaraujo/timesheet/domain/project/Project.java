package com.sauloaraujo.timesheet.domain.project;

import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.IDENTITY;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.sauloaraujo.timesheet.domain.user.User;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
public class Project {
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private Long id;
	
	@NotNull
	@Size(min = 1, max = 50)
	private String name;
	
	@Size(min = 1, max = 200)
	private String description;
	
	@OneToMany(mappedBy = "project", fetch = EAGER)
	private List<Task> tasks;
	
	@ManyToMany
	private List<User> members;
}
package com.sauloaraujo.timesheet.domain.task;

import org.springframework.data.rest.core.config.Projection;

import com.sauloaraujo.timesheet.domain.project.Task;

@Projection(name = "taskProjection", types = Task.class)
public interface TaskProjection {
	long getId();
	String getName();
}
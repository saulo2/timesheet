package com.sauloaraujo.timesheet.domain.entry;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "entryProjection", types = Entry.class)
public interface EntryProjection {
	long getId();
	
	@Value("#{target.task.id}") 
	long getTaskId();
	
	@Value("#{target.date.getTime()}") 
	long getDate();
	
	Double getTime();
}
package com.sauloaraujo.timesheet.domain.project;

import java.util.List;

import org.springframework.data.rest.core.config.Projection;

import com.sauloaraujo.timesheet.domain.task.TaskProjection;

@Projection(name = "projectProjection", types = Project.class)
public interface ProjectProjection {
	String getName();
	List<TaskProjection> getTasks();
}
package com.sauloaraujo.timesheet.ws;

import javax.inject.Inject;
import javax.inject.Named;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Named
public class MessageSender {
	private @Inject ObjectMapper converter;
	
	public void sendMessage(Object objectMessage) {
		String textMessage;
		try {
			textMessage = converter.writer().writeValueAsString(objectMessage);
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
		WebSocket.sendMessage(textMessage);
	}
}
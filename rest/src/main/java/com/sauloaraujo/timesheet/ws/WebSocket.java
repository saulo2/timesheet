package com.sauloaraujo.timesheet.ws;

import java.util.ArrayList;
import java.util.Collection;

import javax.websocket.CloseReason;
import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint(value = "/ws")
public class WebSocket extends Endpoint {
	private static Collection<Session> sessions = new ArrayList<>();

	private static synchronized void addSession(Session session) {
		sessions.add(session);
	}

	private static synchronized Session[] getSessions() {
		return sessions.toArray(new Session[sessions.size()]);
	}
	
	private static synchronized void removeSession(Session session) {
		sessions.remove(session);
	}
	
	public static void sendMessage(String message) {		
		for (Session session : getSessions()) {
			session.getAsyncRemote().sendText(message, result -> {
				if (!result.isOK()) {
					result.getException().printStackTrace();
				}
			});
		}
	}

	@Override
	public void onOpen(Session session, EndpointConfig config) {
		addSession(session);
	}

	@Override
	public void onClose(Session session, CloseReason closeReason) {
		removeSession(session);
	}
}
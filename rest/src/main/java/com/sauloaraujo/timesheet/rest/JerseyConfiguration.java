package com.sauloaraujo.timesheet.rest;

import javax.inject.Named;
import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

@Named
@ApplicationPath("/rest/jersey")
public class JerseyConfiguration extends ResourceConfig {
	public JerseyConfiguration() {
//		register(TimesheetController.class);
//		register(ProjectController.class);
		packages(true,
			JerseyConfiguration.class.getPackage().getName());
		
		register(RolesAllowedDynamicFeature.class);
	}
}
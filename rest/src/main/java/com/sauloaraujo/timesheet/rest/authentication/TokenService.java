package com.sauloaraujo.timesheet.rest.authentication;

import java.security.Key;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Named;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.impl.crypto.MacProvider;

@Named
public class TokenService {
	public static final String ROLES = "roles";
	
	private Key key;
	
	public TokenService() {
		key = MacProvider.generateKey();
		
		String name = "Saulo";
		List<String> roles = Arrays.asList("ADMIN");
		String token = buildToken(name, roles);
		System.out.println(token);
	}
	
	public String buildToken(String name,
			List<String> roles) {
		Map<String, Object> claims = new HashMap<>();
		claims.put(ROLES, roles);
		
		return Jwts.builder()
				.setSubject(name)
				.setClaims(claims)
				.signWith(SignatureAlgorithm.HS256, key)
				.compact();
	}
	
	public Claims parseToken(String token) {
		try {
			return Jwts
					.parser()
					.setSigningKey(key)
					.parseClaimsJws(token)
					.getBody();			
		} catch (SignatureException e) {
			return null;
		}
	}
}

package com.sauloaraujo.timesheet.rest.authentication;

import java.security.Principal;

import javax.ws.rs.core.SecurityContext;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class SecurityContextImpl implements SecurityContext {	
	private PrincipalImpl principal;
	private boolean secure;
	private String authenticationScheme;
	
	@Override
	public Principal getUserPrincipal() {
		return principal;
	}

	@Override
	public boolean isUserInRole(String role) {
		return principal.getRoles().contains(role);
	}

	@Override
	public boolean isSecure() {
		return secure;
	}

	@Override
	public String getAuthenticationScheme() {
		return authenticationScheme;
	}
}

package com.sauloaraujo.timesheet.rest.timesheet;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SaveEntryCellDto {
	private Date date;
	private long taskId;
	private Double time;
}
package com.sauloaraujo.timesheet.rest.project;

import static java.util.stream.Collectors.toList;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.sauloaraujo.timesheet.domain.BusinessException;
import com.sauloaraujo.timesheet.domain.project.Project;
import com.sauloaraujo.timesheet.domain.project.ProjectRepository;
import com.sauloaraujo.timesheet.rest.project.EditProjectResource.ProjectDto;
import com.sauloaraujo.timesheet.rest.project.SearchProjectResource.SearchProjectForm;
import com.sauloaraujo.timesheet.rest.project.SearchProjectResource.SearchProjectForm.SearchProjectOptions;
import com.sauloaraujo.timesheet.rest.project.SearchProjectResource.SearchProjectResult;
import com.sauloaraujo.timesheet.rest.project.SearchProjectResource.SearchProjectResult.ProjectSummary;

@Named
@Path("/project")
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed({"ADMIN"})
public class ProjectController {
	private @Inject ProjectRepository repository;
	
	@GET
	@Path("/search")
	public SearchProjectResource search(@BeanParam SearchProjectOptions options) {
		SearchProjectForm form = new SearchProjectForm();
		form.setOptions(options);
		
		SearchProjectResult result;
		if (options.getSize() != null && options.getIndex() != null) {
			Pageable pageable = new PageRequest(options.getIndex(), options.getSize());			
			Page<Project> page = repository.findAll(pageable);
			
			result = new SearchProjectResult();
			
			result.setProjects(page.getContent().stream().map(project -> {
				ProjectSummary summary = new ProjectSummary();
				summary.setId(project.getId());
				summary.setName(project.getName());
				return summary;
			}).collect(toList()));
						
			result.setTotal(page.getTotalPages());
		} else {
			result = null;
		}
		
		SearchProjectResource resource = new SearchProjectResource();
		resource.setForm(form);
		resource.setResult(result);
		
		return resource;
	}
	
	@GET
	@Path("/{projectId}/edit")
	public EditProjectResource edit(@PathParam("projectId") long projectId) {
		Project project = repository.findOne(projectId);
		if (project == null) {
			throw new NotFoundException();
		}
		
		ProjectDto dto = new ProjectDto();
		dto.setId(project.getId());
		dto.setName(project.getName());
		dto.setDescription(project.getDescription());
		
		EditProjectResource resource = new EditProjectResource();
		resource.setProject(dto);
		
		return resource;
	}

	@POST
	@Path("/{projectId}/save")
	public void save(@PathParam("projectId") long projectId,
			ProjectDto dto) {
		Project project = repository.findOne(projectId);
		if (project == null) {
			throw new NotFoundException();
		}

		project.setName(dto.getName());
		project.setDescription(dto.getDescription());
		
		repository.save(project);
		
		throw new BusinessException("Erro salvando projeto");
	}
}
package com.sauloaraujo.timesheet.rest.timesheet;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ProjectRow {
	private ProjectDto project;
	private List<TaskRow> taskRows;
}
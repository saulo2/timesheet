package com.sauloaraujo.timesheet.rest.authentication;

import java.security.Principal;
import java.util.List;

import io.jsonwebtoken.Claims;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PrincipalImpl implements Principal {
	private Claims claims;
	
	@Override
	public String getName() {
		return claims.getSubject();
	}
	
	public List<String> getRoles() {
		return (List<String>) claims.get(TokenService.ROLES);
	}
}
package com.sauloaraujo.timesheet.rest.timesheet;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ProjectDto {
	private String name;
}
package com.sauloaraujo.timesheet.rest.timesheet;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Timesheet {
	private List<Date> header;
	private List<ProjectRow> projectRows;
}
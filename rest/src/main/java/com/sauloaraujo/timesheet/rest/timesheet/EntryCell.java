package com.sauloaraujo.timesheet.rest.timesheet;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class EntryCell {
	private EntryDto entry;
}
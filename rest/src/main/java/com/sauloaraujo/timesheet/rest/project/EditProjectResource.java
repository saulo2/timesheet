package com.sauloaraujo.timesheet.rest.project;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class EditProjectResource {
	private ProjectDto project;
	
	@Setter
	@Getter
	public static class ProjectDto {
		private Long id;
		private String name;
		private String description;
	}
}
package com.sauloaraujo.timesheet.rest.timesheet;

import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sauloaraujo.timesheet.domain.entry.Entry;
import com.sauloaraujo.timesheet.domain.entry.EntryRepository;
import com.sauloaraujo.timesheet.domain.project.ProjectRepository;
import com.sauloaraujo.timesheet.domain.project.Task;
import com.sauloaraujo.timesheet.domain.project.TaskRepository;
import com.sauloaraujo.timesheet.domain.user.User;
import com.sauloaraujo.timesheet.domain.user.UserRepository;
import com.sauloaraujo.timesheet.ws.MessageSender;

@Named
@Path("/user/{userId}/timesheet")
@Produces(MediaType.APPLICATION_JSON)
public class TimesheetController {
	private @Inject EntryRepository entryRepository;
	private @Inject ProjectRepository projectRepository;
	private @Inject TaskRepository taskRepository;
	private @Inject UserRepository userRepository;
	private @Inject MessageSender messageSender;

	@GET
	@Path("/edit")
	public Timesheet edit(@PathParam("userId") long userId, @QueryParam("start") Long startMiliseconds, @QueryParam("days") Integer days) {
		Date start = startMiliseconds == null ? new Date() : new Date(startMiliseconds);

		if (days == null) {
			days = 7;
		}

		List<Date> header = range(0, days).mapToObj(amount -> {
			Calendar calendar = Calendar.getInstance();

			calendar.setTime(start);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);

			calendar.add(Calendar.DATE, amount);

			return calendar.getTime();
		}).collect(toList());

		Date end = header.get(header.size() - 1);

		List<Entry> entries = entryRepository.findByUserIdAndDateBetween(userId, start, end);

		List<ProjectRow> projectRows = projectRepository.findAll().stream().map(project -> {
			ProjectDto projectDto = new ProjectDto();
			projectDto.setName(project.getName());

			List<TaskRow> taskRows = project.getTasks().stream().map(task -> {
				TaskDto taskDto = new TaskDto();
				taskDto.setId(task.getId());
				taskDto.setName(task.getName());

				List<EntryCell> entryCells = header.stream().map(date -> {
					Double entryTime;

					Optional<Entry> optionalEntry = entries.stream().filter(entry -> {
						return entry.getTask().getId().equals(task.getId()) && entry.getDate().equals(date);
					}).findFirst();

					if (optionalEntry.isPresent()) {
						Entry entry = optionalEntry.get();
						entryTime = entry.getTime();
					} else {
						entryTime = null;
					}

					EntryDto entryDto = new EntryDto();
					entryDto.setTime(entryTime);

					EntryCell entryCell = new EntryCell();
					entryCell.setEntry(entryDto);
					return entryCell;
				}).collect(toList());

				TaskRow taskRow = new TaskRow();
				taskRow.setTask(taskDto);
				taskRow.setEntryCells(entryCells);
				return taskRow;
			}).collect(toList());

			ProjectRow projectRow = new ProjectRow();
			projectRow.setProject(projectDto);
			projectRow.setTaskRows(taskRows);
			return projectRow;
		}).collect(toList());

		Timesheet timesheet = new Timesheet();
		timesheet.setHeader(header);
		timesheet.setProjectRows(projectRows);
		return timesheet;
	}

	@POST
	@Path("/save")
	@Transactional
	public void save(@PathParam("userId") long userId, Timesheet timesheet) {
		List<Date> header = timesheet.getHeader();

		timesheet.getProjectRows().forEach(projectRow -> {
			projectRow.getTaskRows().forEach(taskRow -> {
				long taskId = taskRow.getTask().getId();

				List<EntryCell> entryCells = taskRow.getEntryCells();
				for (int i = 0; i < entryCells.size(); ++i) {
					Date date = header.get(i);
					Entry entry = entryRepository.findByUserIdAndTaskIdAndDate(userId, taskId, date);

					EntryCell entryCell = entryCells.get(i);
					if (entryCell != null) {
						EntryDto entryDto = entryCell.getEntry();
						Double time = entryDto.getTime();
						if (time == null || time == 0) {
							if (entry != null) {
								entryRepository.delete(entry);
							}
						} else {
							if (entry == null) {
								User user = userRepository.getOne(userId);
								Task task = taskRepository.getOne(taskId);

								entry = new Entry();
								entry.setUser(user);
								entry.setTask(task);
								entry.setDate(date);
							}

							entry.setTime(time);
							entryRepository.save(entry);
						}
					}
				}
			});
		});
		
		messageSender.sendMessage(timesheet);
	}
	
	@POST
	@Path("/save-entry-cell")
	public void saveCell(@PathParam("userId") long userId, SaveEntryCellDto dto) {
		Entry entry = entryRepository.findByUserIdAndTaskIdAndDate(userId, dto.getTaskId(), dto.getDate());
		if (entry == null) {
			User user = userRepository.findOne(userId);
			Task task = taskRepository.findOne(dto.getTaskId());
			
			entry = new Entry();
			entry.setUser(user);
			entry.setTask(task);
			entry.setDate(dto.getDate());
		}
		
		entry.setTime(dto.getTime());

		entryRepository.save(entry);
	}	
}
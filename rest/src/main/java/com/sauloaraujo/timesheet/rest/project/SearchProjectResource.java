package com.sauloaraujo.timesheet.rest.project;

import java.util.List;

import javax.ws.rs.QueryParam;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SearchProjectResource {
	private SearchProjectForm form;
	private SearchProjectResult result;
	
	@Setter
	@Getter
	public static class SearchProjectForm {
		private SearchProjectOptions options;

		@Setter
		@Getter		
		public static class SearchProjectOptions {
			private @QueryParam("name") String name;
			private @QueryParam("description") String description;
			private @QueryParam("size") Integer size;
			private @QueryParam("index") Integer index;
		}
	}
	
	@Setter
	@Getter
	public static class SearchProjectResult {
		private List<ProjectSummary> projects;
		private long total;

		@Setter
		@Getter		
		public static class ProjectSummary {
			private long id;
			private String name;
		}
	}	
}
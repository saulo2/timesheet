package com.sauloaraujo.timesheet.rest.timesheet;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TaskRow {
	private TaskDto task;
	private List<EntryCell> entryCells;
}
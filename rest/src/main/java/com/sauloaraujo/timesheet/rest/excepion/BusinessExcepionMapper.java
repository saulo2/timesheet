package com.sauloaraujo.timesheet.rest.excepion;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.sauloaraujo.timesheet.domain.BusinessException;

@Provider
public class BusinessExcepionMapper 
		implements ExceptionMapper<BusinessException> {
	@Override
	public Response toResponse(BusinessException exception) {
		return Response
			.status(Status.PRECONDITION_FAILED)
			.entity(exception.getMessage())
			.build();
	}
}
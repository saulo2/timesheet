package com.sauloaraujo.timesheet.rest.authentication;

import java.io.IOException;
import java.util.StringTokenizer;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.ext.Provider;

import io.jsonwebtoken.Claims;

@Named
@Provider
@PreMatching
public class AuthenticationFilter 
	implements ContainerRequestFilter {
	
	private @Inject TokenService service;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		String header = requestContext.getHeaderString("authorization");
		if (header != null) {
			StringTokenizer tokenizer = new StringTokenizer(header, " ");
			if (tokenizer.hasMoreTokens()) {
				String authenticationScheme = tokenizer.nextToken();
				if ("Bearer".equals(authenticationScheme)) {
					if (tokenizer.hasMoreTokens()) {
						String token = tokenizer.nextToken();
						Claims claims = service.parseToken(token);
						if (claims != null) {
							PrincipalImpl principal = new PrincipalImpl(claims);

							String uriScheme = requestContext.getUriInfo().getRequestUri().getScheme();
							boolean secure = "https".equals(uriScheme);

							SecurityContextImpl securityContext = new SecurityContextImpl(principal, secure, authenticationScheme)	;

							requestContext.setSecurityContext(securityContext);
						}
					}
				}	
			}	
		}
	}
}
package com.sauloaraujo.timesheet.rest.timesheet;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TaskDto {
	private long id;
	private String name;
}